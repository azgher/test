const assert = require('assert')

const app = require('../src/app.js')
const app2 = require('../src/app2.js')

describe('tests', () => {
  it('should test all (sync)', () => {
    assert.strictEqual(app.sum(1, 2), 3)

    assert.strictEqual(app.subtract(1, 2), -1)

    assert.strictEqual(app.memory.size(), 0)

    assert.strictEqual(app.memory.remember(), 0[0])

    assert.strictEqual(app.memory.size(), 0)

    assert.strictEqual(app.memory.keep(1, 1), 0[0])

    assert.strictEqual(app.memory.size(), 1)

    assert.strictEqual(app.memory.remember(1), 1)

    assert.strictEqual(app.memory.size(), 1)

    assert.strictEqual(app.memory.forget(1), 0[0])

    assert.strictEqual(app.memory.size(), 0)

    assert.strictEqual(app.uncoveredFunc(2, 3), 6)

    assert.strictEqual(app.coverage(true), 'covered')

    assert.strictEqual(app.coverage(false), 'not covered')

    assert.strictEqual(app2.func(1, 1), 1)

    assert.strictEqual(app2.func(0, 1), 1)

    assert.strictEqual(app2.func(0, 0), 0)
  })

  it('should test all (async)', async () => {
    await assert.doesNotReject(app2.wait(3))

    await assert.rejects(app2.wait(0))

    const promise = app2.wait(3)

    await assert.doesNotReject(promise.abort())
  })
})

