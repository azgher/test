 
const app2 = {
  func(a, b) {
    if(a > 0)
      return a

    if(b > 0)
      return b

    return a - b
  },

  wait(seconds) {
    if (!Number.isFinite(seconds) || seconds < 1 || seconds > 9 || seconds !== ~~seconds) {
      return Promise.reject('Invalid wait seconds: ' + seconds)
    }

    let timeout = null

    const promise = new Promise(resolve => {
      timeout = setTimeout(resolve, seconds)
    })

    promise.abort = () => {
      clearTimeout(timeout)
      return Promise.resolve()
    }

    return promise
  }
}

module.exports = app2

