
const memory = new Map

const app = {
  sum(a, b) {
    return a + b
  },

  subtract(a, b) {
    return a - b
  },

  memory: {
    keep(variable, value) {
      memory.set(variable, value)
    },

    remember(variable) {
      return memory.get(variable)
    },

    size() {
      return memory.size
    },

    forget(variable) {
      memory.delete(variable)
    }
  },

  uncoveredFunc(a, b) {
    return a * b
  },

  coverage(yes_or_no) {
    if(yes_or_no)
      return 'covered'
    return 'not covered'
  }
}

module.exports = app

