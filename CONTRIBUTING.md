# Contributing

Open an issue or pull request if you want to contribute. There are no IQ tests and you don't have share my way of life, its fine.

Just explain the best you can what is the problem you are trying to solve and we'll talk about it in the comments thread.